import Layout from '../components/Layout';
import styles from '../styles/Blog.module.css';

interface Posts {
  id: number,
  title: string,
  body: string,
}

interface DataBlog {
  blog: Posts[]
}

export default function BLog(props: DataBlog) {
  const {blog} = props;
  return (
    <>
      <Layout pageTitle="Blog Page">
        {blog.map((item) => (
          <div key={item.id} className={styles.card}>
            <h3>{item.title}</h3>
            <p>{item.body}</p>
          </div>
        ))}
      </Layout>
    </>
  );
}

export async function getServerSideProps() {
  const response = await fetch(`https://jsonplaceholder.typicode.com/posts`);
  const blog = await response.json();
  return {
    props: {
      blog,
    },
  };
}
