import type { NextPage } from 'next';
import Image from 'next/image';
import styles from '../styles/Home.module.css';
import Layout from '../components/Layout';

const Home: NextPage = () => (
  <>
    <Layout pageTitle="Home Page">
      <Image src="/banner-1.jpg" width={200} height={200} alt="Banner"/>
      <h1 className={styles['title-homepage']}>Ghina Wishaliani Putri</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <Image src="/kelas.png" width={200} height={200} alt="Kelas"/>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, molestias.</p>
      <Image src="/bebas.jpg" width={200} height={200} alt="bebas"/>
    </Layout>
  </>
);

export default Home;
